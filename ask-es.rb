#!/usr/bin/env ruby

require 'elasticsearch'
require 'docopt'
require 'json'

# Run a query against elasticsearch
# Type of query: full result set, selected fields or document count for just the hit count
# Query: a lucene formatted query
# Fields: returns an array of hits containing only selected fields
# Start/End: restrict the time interval - default NOW - 6h
# Can save results to csv file

TIME_RANGE=6*3600

doc = <<DOCOPT

Usage:
  #{__FILE__} -h | --help
  #{__FILE__} [--query=<query>] --count [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--debug]
  #{__FILE__} [--query=<query>] --count [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] --start=<tstart> [--end=<tend>] [--debug]
  #{__FILE__} [--query=<query>] [--id ] [--fields=<fields>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--debug]
  #{__FILE__} [--query=<query>] [--id ] [--fields=<fields>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] --start=<tstart> [--end=<tend>] [--debug]
  #{__FILE__} [--query=<query>] [--id ]  --fields=<fields>  [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--csv] [--debug]
  #{__FILE__} [--query=<query>] [--id ]  --fields=<fields>  [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--csv] --start=<tstart> [--end=<tend>] [--debug]
  #{__FILE__} --agg=<agg> [--query=<query>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] (--csv|--uniq) [--debug]
  #{__FILE__} --agg=<agg> [--query=<query>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] (--csv|--uniq) --start=<tstart> [--end=<tend>] [--debug]

Options:
  -h --help            This script runs a query against elasticsearch (logstash). It can return a count, only specific fields or a full result set.
  --query=<query>      The query to execute in Lucene syntax i.e. --query="state:Sweden NOT city:Kiruna" [default: *].
  --count              Specify if you only want the count back [default: false].
  --fields=<fields>    Comma delimited list of fields you want to be returned back [default: ].
  --id                 Return the id of the document [default: false].
  --debug              Verbose logging [default: false].
  --start=<tstart>     Start time for search (YYYY-MM-DD or YYYY-MM-DDTHH:MM:SS or -[0-9]{1-3}[dhms]{1} i.e. -300s. If omitted the default time range is 6h [default: ].
  --end=<tend>         End time for search (YYYY-MM-DD or YYYY-MM-DDTHH:MM:SS) if omitted we assume NOW [default: ].
  --index_prefix=<index_prefix>  Index prefix to use [default: logstash].
  --es_endpoint=<es_endpoint>  Elasticsearch node IP or DNS name [default: localhost:9200].
  --size=<size>        Return <size> results [default: 50].
  --csv                Return the results in csv format, only in combination with --fields or --agg [default: false].
  --agg=<agg>          Return the aggregation result on field <agg>.
  --uniq               When aggregating just return the count of uniq terms [default: false].

DOCOPT

begin
  require "pp"
  options = Docopt::docopt(doc)
rescue Docopt::Exit => e
  puts e.message
end

index_prefix = options['--index_prefix']
es_endpoint = options['--es_endpoint']
query = options['--query']
count = options['--count']
fields = options['--fields']
tstart = options['--start']
tend = options['--end']
debug = options['--debug']
id = options['--id']
size = options['--size'].to_i
csv = options['--csv']
agg = options['--agg']
uniq = options['--uniq']

pp options if debug

def fmt_date(d)
  begin
    # This assumes the date is passed in as YYYY-mm-ddTHH:MM:SS or -600s if a start date
    dsec = d.split('T')[1]
    dday = d.split('T')[0]
    dsec.nil? ? (dhh,dmm,dss = 00, 00, 00) : (dhh,dmm,dss = dsec.split(':')[0].to_i,dsec.split(':')[1].to_i,dsec.split(':')[2].to_i)
    y,m,d = dday.split('-')[0],dday.split('-')[1],dday.split('-')[2]
  rescue Exception => e
    puts "ERROR while formatting date #{d}, date must be passed as YYYY-MM-DD or YYYY-MM-DDTHH:MM:SS, #{e.message}"
    raise e
  end
  return y,m,d,dhh,dmm,dss
end

def get_relative_date(d,r)
  raise "Malformatted relative date #{d}. Correct format: -[0-9]{1-3}[dhms]{1}" unless d.match(/^-[0-9]{1,3}[mhsd]{1}$/)
  measure = d[-1..d.length]
  case measure
    when 'h'
      multiplier = 3600
    when 'm'
      multiplier = 60
    when 'd'
      multiplier = 24*3600
    else
      multiplier = 1
  end

  count = d[1..-2].to_i

  r.to_i - (count*multiplier)
end

# Generate the end time if needed
time_now = Time.now.utc.strftime("%Y-%m-%dT%H:%M:%S")
y,m,d,dhh,dmm,dss = fmt_date(time_now)
time_now = Time.new(y,m,d, dhh,dmm,dss, '+00:00').utc.to_i

if tend == ""
  tend = time_now
else
  if tend.match(/^-[0-9]{1,3}[mhsd]{1}$/)
    tend = get_relative_date(tend,time_now)
  else
    y,m,d,dhh,dmm,dss = fmt_date(tend)
    tend = Time.new(y,m,d, dhh,dmm,dss, '+00:00').utc.to_i
  end
end

# Build the time interval
if tstart != ""

  puts "Range query requested" if debug

  # The user can pass --start=-3h
  if tstart.match(/^-[0-9]{1,3}[mhsd]{1}$/)
    tstart = get_relative_date(tstart,tend)
    puts "Time range #{tstart} - #{tend}" if debug
  else
    y,m,d,dhh,dmm,dss = fmt_date(tstart)
    tstart = Time.new(y,m,d, dhh,dmm,dss, '+00:00').utc.to_i
  end

else
  tstart = tend - TIME_RANGE
end

time_to = Time.at(tend).utc.strftime("%Y-%m-%dT%H:%M:%S.000Z")
time_from = Time.at(tstart).utc.strftime("%Y-%m-%dT%H:%M:%S.000Z")

puts "End time: #{time_to}" if debug
puts "Start time: #{time_from}"  if debug

# Build the query
# Fix query if enclosed in single quotes as it breaks the search

query = query.chop.reverse.chop.reverse unless query.match(/'.*'/).nil?
query = "@timestamp:[#{time_from} TO #{time_to}] AND #{query}"


# Get a connection
@esclient = Elasticsearch::Client.new  host: "#{es_endpoint}", log: false, reload_on_failure: true, retry_on_failure: 5


begin
  # Only run a count query
  if count
    puts "Count query" if debug
    es_result = @esclient.search index: "#{index_prefix}*", q: query, search_type: 'count'
    puts es_result['hits']['total'].to_s
  # If we have passed --fields then return it, also include _id if required
  # If we have only passed --id but no fields just return a list of the ids
  elsif fields != "" || id
    fields = fields.chop.reverse.chop.reverse unless fields.match(/'.*'/).nil?
    afields = fields.split(',')
    puts "Only returning fields: #{fields} OR #{afields}" if debug
    es_result = @esclient.search index: "#{index_prefix}*", q: query, fields: "#{fields}", size: size
    results = es_result['hits']['hits']
    puts results.inspect if debug
    survivors = ['fields']
    survivors << '_id' if id
    survived_results = []
    results.each do |r|
      r.each do |k,_|
        r.delete(k) unless survivors.include?(k)
      end
    survived_results << r['fields']
    end
 
    if csv
      puts "Returning csv for the requested fields" if debug
      require 'tempfile'
      require 'csv'
      destfile = Dir::Tmpname.create(['es_result','.csv'], File.expand_path('~')) { |path| puts "Storing results in csv format in #{path}" }
      CSV.open(destfile, "wb") do |c|
        c << afields.sort
        survived_results.each do |s|
          csv_arr = []
          afields.sort.each do |f|
            csv_arr << s[f].join(':')
          end
          c << csv_arr
        end
      end
    else
      puts survived_results.to_json
    end

  # Otherwise we want back the full result
  else
    if agg.nil?
      puts "Returning full result set" if debug
      es_result = @esclient.search index: "#{index_prefix}*", q: query, size: size
      puts es_result.to_json
    else
      puts "Returning aggregate result set" if debug
      es_result = @esclient.search index: "#{index_prefix}*", q: query, body: {facets: { agg => { terms: { field: "#{agg}", size: size } }  }, fields: "" }
      # Note - the docopt options only allow --agg with either --csv OR --uniq
      # It's not possible to get here and have a query that is not
      if csv
        puts "Returning csv" if debug
        require 'tempfile'
        require 'csv'
        destfile = Dir::Tmpname.create(['es_result','.csv'], File.expand_path('~')) { |path| puts "Storing results in csv format in #{path}" }
        CSV.open(destfile, "wb") do |c|
          c << [ agg, 'count' ]
          es_result['facets'][agg]['terms'].each do |j|
            c << [ j['term'] , j['count'].to_s ]
          end
        end
      elsif uniq
        puts "Returning uniq count" if debug
        puts es_result['facets'][agg]['terms'].count.to_s
      else
        puts es_result.to_json
      end
    end
  end
rescue Exception => e
  puts "ERROR: Failed to run the ES query, #{e.message}"
  raise e
end

