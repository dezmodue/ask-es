# README #

Simple script to query Elastic, it can return a count, a full result set, selected fields, generate a csv file, return aggregations on one field. The query uses the lucene syntax

### How do I get set up? ###

This script requires the gems elasticsearch, docopt and json, either install them manually or use bundle install 

### Example ###

```
#!shell

 $ ruby ask-es.rb --help
Usage:
  ask-es.rb -h | --help
  ask-es.rb [--query=<query>] --count [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--debug]
  ask-es.rb [--query=<query>] --count [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] --start=<tstart> [--end=<tend>] [--debug]
  ask-es.rb [--query=<query>] [--id ] [--fields=<fields>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--debug]
  ask-es.rb [--query=<query>] [--id ] [--fields=<fields>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] --start=<tstart> [--end=<tend>] [--debug]
  ask-es.rb [--query=<query>] [--id ]  --fields=<fields>  [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--csv] [--debug]
  ask-es.rb [--query=<query>] [--id ]  --fields=<fields>  [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] [--csv] --start=<tstart> [--end=<tend>] [--debug]
  ask-es.rb --agg=<agg> [--query=<query>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] (--csv|--uniq) [--debug]
  ask-es.rb --agg=<agg> [--query=<query>] [--index_prefix=<index_prefix>] [--es_endpoint=<es_endpoint>] [--size=<size>] (--csv|--uniq) --start=<tstart> [--end=<tend>] [--debug]

Options:
  -h --help            This script runs a query against elasticsearch (logstash). It can return a count, only specific fields or a full result set.
  --query=<query>      The query to execute in Lucene syntax i.e. --query="state:Sweden NOT city:Kiruna" [default: *].
  --count              Specify if you only want the count back [default: false].
  --fields=<fields>    Comma delimited list of fields you want to be returned back [default: ].
  --id                 Return the id of the document [default: false].
  --debug              Verbose logging [default: false].
  --start=<tstart>     Start time for search (YYYY-MM-DD or YYYY-MM-DDTHH:MM:SS or -[0-9]{1-3}[dhms]{1} i.e. -300s. If omitted the default time range is 6h [default: ].
  --end=<tend>         End time for search (YYYY-MM-DD or YYYY-MM-DDTHH:MM:SS) if omitted we assume NOW [default: ].
  --index_prefix=<index_prefix>  Index prefix to use [default: logstash].
  --es_endpoint=<es_endpoint>  Elasticsearch node IP or DNS name [default: localhost:9200].
  --size=<size>        Return <size> results [default: 50].
  --csv                Return the results in csv format, only in combination with --fields or --agg [default: false].
  --agg=<agg>          Return the aggregation result on field <agg>.
  --uniq               When aggregating just return the count of uniq terms [default: false].
```